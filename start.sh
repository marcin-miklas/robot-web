#!/usr/bin/env bash
ssh -L 12345:localhost:5000 pi@robotpi.local 'git -C robot/web pull; killall flask; FLASK_APP=robot/web/robot-web/robot.py flask run --host=0.0.0.0 2>&1 | grep -v 200'
