#!/usr/bin/python3
# encoding=utf-8
from __future__ import print_function

import time
from threading import Thread, Event

import bakebit
from FriendlyELEC_NanoHatMotor import FriendlyELEC_NanoHatMotor as NanoHatMotor
from flask import Flask, render_template, request


class Motor(object):
    controller = NanoHatMotor(addr=0x60)

    def __init__(self, number):
        self.motor = Motor.controller.getMotor(number)
        self._speed = 0

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, new_speed):
        new_speed = max(-255, min(new_speed, 255))
        if new_speed == self._speed:
            return
        if new_speed > 0:
            self.motor.setSpeed(int(new_speed))
            if not self._speed > 0:
                self.motor.run(NanoHatMotor.FORWARD)
        elif new_speed < 0:
            self.motor.setSpeed(int(-new_speed))
            if not self._speed < 0:
                self.motor.run(NanoHatMotor.BACKWARD)
        else:  # new_speed == 0
            if not self._speed == 0:
                self.motor.run(NanoHatMotor.RELEASE)
        self._speed = new_speed


class Sensors(Thread):
    ultrasonic_ranger = 4
    sound_sensor = 0

    def __init__(self):
        Thread.__init__(self)
        self.distance = 0
        bakebit.pinMode(self.sound_sensor, "INPUT")
        self.volume = 0
        self.running = Event()
        self.running.set()

    def run(self):
        while self.running.is_set():
            try:
                distance = bakebit.ultrasonicRead(self.ultrasonic_ranger)
                if distance == 0 or distance > 9999:
                    self.distance = 9999
                else:
                    self.distance = distance
                time.sleep(.1)
                self.volume = bakebit.analogRead(self.sound_sensor)
                time.sleep(.1)
            except (TypeError, IOError) as err:
                pass

    def stop(self):
        self.running.clear()


app = Flask(__name__)

left = Motor(4)
right = Motor(1)

sensors = Sensors()
sensors.start()


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/move")
def move():
    speed = int(request.args['speed'])
    angle = int(request.args['angle'])
    if -5 < angle < 5:
        left.speed += speed
        right.speed += speed
    elif angle > 0:
        left.speed += angle
        right.speed -= angle
    else:
        left.speed -= angle
        right.speed += angle

    return ""


@app.route("/stop")
def stop():
    left.speed = 0
    right.speed = 0
    return ""


###### Scratch2 API ######
led = 3
buzzer = 5


@app.route("/poll")
def poll():
    return "distance {}\n" \
           "volume {}\n".format(
        sensors.distance,
        sensors.volume)


@app.route("/reset_all")
def reset_all():
    left.speed = 0
    right.speed = 0
    bakebit.digitalWrite(led, 0)
    bakebit.analogWrite(buzzer, 0)
    return ""


@app.route("/leftMotor/<speed>")
def leftMotor(speed):
    left.speed = int(speed)
    return ""


@app.route("/rightMotor/<speed>")
def rightMotor(speed):
    right.speed = int(speed)
    return ""


@app.route("/stopMotors")
def stopMotors():
    left.speed = 0
    right.speed = 0
    return ""


@app.route("/forward/<speed>")
def forward(speed):
    left.speed = int(speed)
    right.speed = int(speed)
    return ""


@app.route("/backward/<speed>")
def backward(speed):
    left.speed = -int(speed)
    right.speed = -int(speed)
    return ""


@app.route("/rotateLeft/<speed>")
def rotateLeft(speed):
    left.speed = -int(speed)
    right.speed = int(speed)
    return ""


@app.route("/rotateRight/<speed>")
def rotateRight(speed):
    left.speed = int(speed)
    right.speed = -int(speed)
    return ""


@app.route("/lightOn")
def lightOn():
    bakebit.digitalWrite(led, 1)
    return ""


@app.route("/lightOff")
def lightOff():
    bakebit.digitalWrite(led, 0)
    return ""


@app.route("/startBuzz/<int:val>")
def startBuzz(val):
    bakebit.analogWrite(buzzer, val)
    return ""


@app.route("/stopBuzz")
def stopBuzz():
    bakebit.analogWrite(buzzer, 0)
    return ""


#############################


if __name__ == '__main__':
    from flask.cli import main
    import sys
    import os

    os.environ['FLASK_APP'] = sys.argv[0]
    os.environ['FLASK_DEBUG'] = '1'
    sys.argv.append('run')
    sys.argv.append('--host=0.0.0.0')
    main(True)
