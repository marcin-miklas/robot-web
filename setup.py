from setuptools import setup

setup(name="robot-web",
      version='0.1',
      description="Web page for robot steering",
      author="Marcin Miklas",
      author_email="marcin@miklas.pl",
      packages=["robot-web"],
      install_requires=["Flask"],
      )
